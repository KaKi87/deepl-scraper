(async () => {

    const t1 = Date.now();

    const
        consola = require('consola'),
        axios = require('axios');

    const { getSupportedLanguages, translate, quit } = require('.');

    const eventCount = {
        total: 0,
        success: 0,
        warning: 0,
        error: 0
    };

    const success = (...args) => {
        eventCount.total++;
        eventCount.success++;
        consola.success(...args);
    };

    const warning = (...args) => {
        eventCount.total++;
        eventCount.warning++;
        consola.warn(...args);
    };

    const error = (...args) => {
        eventCount.total++;
        eventCount.error++;
        consola.error(...args);
    };

    consola.info('Starting test suite');

    consola.info('Testing supported languages');

    const { sourceLanguages, targetLanguages } = await getSupportedLanguages();

    if(Array.isArray(sourceLanguages) && sourceLanguages.every(sourceLanguage => /^[a-z]{2}$/.test(sourceLanguage)))

        success('Source languages are valid');

    else

        return error('Source languages are invalid');

    if(Array.isArray(targetLanguages) && targetLanguages.every(targetLanguage => /^[a-z]{2}-[A-Z]{2}$/.test(targetLanguage)))

        success('Target languages are valid');

    else

        return error('Target languages are invalid');

    consola.info('Downloading pangrams');

    let pangrams;

    try {

        pangrams = (await axios('https://raw.githubusercontent.com/hyvyys/language-data/master/dist/language-data.json'))
            .data.reduce((res, item) => Object.assign(res, { [item['htmlTag']]: item['pangrams'][0] }), {});

    } catch(_){

        return error('Pangrams download failed');

    }

    consola.info('Testing translations');

    const total = sourceLanguages.length * targetLanguages.length;
    let n = 0;

    for(let sourceLanguageIndex = 0; sourceLanguageIndex < sourceLanguages.length; sourceLanguageIndex++){

        const sourceLanguage = sourceLanguages[sourceLanguageIndex];

        const pangram = pangrams[sourceLanguage];

        if(!pangram){

            warning(`No pangram available for ${sourceLanguage}`);

            continue;

        }

        consola.info(`Testing translations from ${sourceLanguage}`);

        for(let targetLanguageIndex = 0; targetLanguageIndex < targetLanguages.length; targetLanguageIndex++){

            const targetLanguage = targetLanguages[targetLanguageIndex];

            const shortTargetLanguage = targetLanguage.split('-')[0];

            if(sourceLanguage === shortTargetLanguage) continue;

            consola.info(`Testing translation from ${sourceLanguage} to ${targetLanguage} (${++n}/${total} ~ ${((n / total) * 100).toFixed(2)}%)`);

            let translation;

            try {

                translation = await translate(pangram, sourceLanguage, targetLanguage);

            }

            catch(_){

                error('Translation failed');

                continue;

            }

            const { source, target } = translation;

            if(source.lang !== sourceLanguage)

                error('Result source language does not match request source language');

            else if(target.lang !== shortTargetLanguage)

                error('Result target language does not match request target language');

            else

                success(`Translation : ${target.translation}`);

        }

    }

    consola.info('Quitting');

    await quit();

    consola.info('Test suite completed with '
        + `${eventCount.success} successes (${(eventCount.success / eventCount.total * 100).toFixed(0)}%), `
        + `${eventCount.warning} warnings (${(eventCount.warning / eventCount.total * 100).toFixed(0)}%), and `
        + `${eventCount.error} errors (${(eventCount.error / eventCount.total * 100).toFixed(0)}%) `
        + `in ${(Date.now() - t1) / 1000} seconds.`
    );

})();